function User(
  taiKhoan,
  hoTen,
  email,
  matKhau,
  ngayLam,
  luongCB,
  chucVu,
  gioLam
) {
  this.taiKhoan = taiKhoan;
  this.hoTen = hoTen;
  this.email = email;
  this.matKhau = matKhau;
  this.ngayLam = ngayLam;
  this.luong = luongCB;
  this.chucVu = chucVu;
  this.gioLam = gioLam;
  this.tongLuong = function () {
    if (this.chucVu == "Sếp") {
      return this.luong * 3;
    } else if (this.chucVu == "Trưởng phòng") {
      return this.luong * 2;
    } else {
      return this.luong;
    }
  };
  this.xepLoai = function () {
    if (this.gioLam >= 192) {
      return "Nhân Viên Xuất Sắc";
    } else if (this.gioLam >= 176) {
      return "Nhân Viên Giỏi";
    } else if (this.gioLam >= 160) {
      return "Nhân Viên Khá";
    } else {
      return "Nhân Viên Trung Bình";
    }
  };
}
