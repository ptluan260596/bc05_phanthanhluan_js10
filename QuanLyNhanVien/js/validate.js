function kiemTraTrung(id, dssv) {
  let index = timKiemViTri(id, dssv);
  if (index !== -1) {
    showMessageErr("tbTKNV", "Tài khoản nhân viên đã trùng");
    return false;
  } else {
    showMessageErr("tbTKNV", "");
    return true;
  }
}
function kiemTraRong(userInput, idErr, message) {
  if (userInput.length == 0) {
    showMessageErr(idErr, message);
    return false;
  } else {
    showMessageErr(idErr, "");
    return true;
  }
}
function kiemTraKyTu(value, idErr) {
  var reg = /^[0-9]{4,6}$/;
  let isCharacter6 = reg.test(value);
  if (isCharacter6) {
    showMessageErr(idErr, "");
    return true;
  } else {
    showMessageErr(idErr, "Tài khoản tối đa từ 4-6 ký tự");
    return false;
  }
}

function kiemTraEmail(value, idErr) {
  var reg =
    /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  let isEmail = reg.test(value);
  if (isEmail) {
    showMessageErr(idErr, "");
    return true;
  } else {
    showMessageErr(idErr, "Email không đúng định dạng");
    return false;
  }
}

function kiemTraMatKhau(value, idErr) {
  var reg = /^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{6,10}$/;
  let isPass = reg.test(value);
  if (isPass) {
    showMessageErr(idErr, "");
    return true;
  } else {
    showMessageErr(idErr, "Mật khẩu không đúng định dạng");
    return false;
  }
}
function kiemTraTen(value, idErr) {
  //   var reg = /^(([A-Za-z])*([A-Za-z]+)?\s)+([A-Za-z])*([A-Za-z]+)?$/;
  var reg =
    /^[a-zA-ZÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂẾưăạảấầẩẫậắằẳẵặẹẻẽềềểếỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ\s\W|_]/;
  let isName = reg.test(value);
  if (isName) {
    showMessageErr(idErr, "");
    return true;
  } else {
    showMessageErr(idErr, "Tên nhân viên phải là chữ");
    return false;
  }
}

function kiemTraNgayLam(value, idErr) {
  var reg =
    /^((0?[1-9]|1[012])[- /.](0?[1-9]|[12][0-9]|3[01])[- /.](19|20)?[0-9]{2})*$/;
  let isDate = reg.test(value);
  if (isDate) {
    showMessageErr(idErr, "");
    return true;
  } else {
    showMessageErr(idErr, "Ngày làm có định dạng mm/dd/yyyy");
    return false;
  }
}

function kiemTraLuong(value, idErr) {
  var firstNumber = 1000000;
  var lastNumber = 20000000;
  var reg = value;
  if (firstNumber <= reg && reg <= lastNumber) {
    showMessageErr(idErr, "");
    return true;
  } else {
    showMessageErr(idErr, "Lương từ 1.000.000 đến 20.000.000");
    return false;
  }
}

function kiemTraChucVu(value, idErr) {
  let isChucVu = document.getElementById("chucvu").value;
  if (
    isChucVu == "Sếp" ||
    isChucVu == "Trưởng phòng" ||
    isChucVu == "Nhân viên"
  ) {
    showMessageErr(idErr, "");
    return true;
  } else {
    showMessageErr(idErr, "Chọn chức vụ hợp lệ");
    return false;
  }
}

function kiemTraGioLam(value, idErr) {
  let isGioLam = document.getElementById("gioLam").value * 1;
  if (isGioLam >= 80 && isGioLam <= 200) {
    showMessageErr(idErr, "");
    return true;
  } else {
    showMessageErr(idErr, " Giờ làm cơ bản từ 80 - 200 ");
    return false;
  }
}
