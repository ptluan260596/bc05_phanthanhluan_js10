function layThongTinTuForm() {
  var taiKhoan = document.getElementById("tknv").value;
  var hoTen = document.getElementById("name").value;
  var email = document.getElementById("email").value;
  var matKhau = document.getElementById("password").value;
  var ngayLam = document.getElementById("datepicker").value;
  var luongCB = document.getElementById("luongCB").value;
  var chucVu = document.getElementById("chucvu").value;
  var gioLam = document.getElementById("gioLam").value;
  var user = new User(
    taiKhoan,
    hoTen,
    email,
    matKhau,
    ngayLam,
    luongCB,
    chucVu,
    gioLam
  );
  return user;
}

function renderDSNV(nvArr) {
  var contentHTML = "";
  for (var index = 0; index < nvArr.length; index++) {
    var currentNV = nvArr[index];
    var contentTr = `<tr>
    <th>${currentNV.taiKhoan}</th>
    <th>${currentNV.hoTen}</th>
    <th>${currentNV.email}</th>
    <th>${currentNV.ngayLam}</th>
    <th>${currentNV.chucVu}</th>
    <th>${currentNV.tongLuong()}</th>
    <th>${currentNV.xepLoai()}</th>
    <th>
    <button class="btn btn-danger my-2" onclick="xoaNV('${
      currentNV.taiKhoan
    }')">Xoá</button>
    <button class="btn btn-warning" onclick="suaNV('${
      currentNV.taiKhoan
    }')" data-toggle="modal" data-target="#myModal">Sửa</button>
    </th>
    </tr>
    `;
    contentHTML += contentTr;
  }
  document.getElementById("tableDanhSach").innerHTML = contentHTML;
}

function timKiemViTri(id, nvArr) {
  for (var index = 0; index < nvArr.length; index++) {
    var item = nvArr[index];
    if (item.taiKhoan == id) {
      return index;
    }
  }
  return -1;
}
function showThongTinLenForm(nv) {
  document.getElementById("tknv").value = nv.taiKhoan;
  document.getElementById("name").value = nv.hoTen;
  document.getElementById("email").value = nv.email;
  document.getElementById("password").value = nv.matKhau;
  document.getElementById("datepicker").value = nv.ngayLam;
  document.getElementById("luongCB").value = nv.luong;
  document.getElementById("chucvu").value = nv.chucVu;
  document.getElementById("gioLam").value = nv.gioLam;
}
function resetForm() {
  document.getElementById("formQLNV").reset();
}

function showMessageErr(idErr, massage) {
  document.getElementById(idErr).innerHTML = massage;
}
