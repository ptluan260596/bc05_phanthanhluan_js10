var dsnv = [];
const DSNV = "DSNV";

function luuLocalStorage() {
  let jsonDsnv = JSON.stringify(dsnv);
  localStorage.setItem(DSNV, jsonDsnv);
}

var dataJson = localStorage.getItem(DSNV);
if (dataJson !== null) {
  var nvArr = JSON.parse(dataJson);
  for (var index = 0; index < nvArr.length; index++) {
    var item = nvArr[index];
    var nv = new User(
      item.taiKhoan,
      item.hoTen,
      item.email,
      item.matKhau,
      item.ngayLam,
      item.luong,
      item.chucVu,
      item.gioLam
    );
    dsnv.push(nv);
  }
  renderDSNV(dsnv);
}

function themNV() {
  var user = layThongTinTuForm();
  var isValid = true;
  isValid =
    kiemTraTrung(user.taiKhoan, dsnv) &&
    kiemTraRong(user.taiKhoan, "tbTKNV", "Tài khoản không được để trống") &&
    kiemTraKyTu(user.taiKhoan, "tbTKNV");
  isValid =
    isValid & kiemTraTen(user.hoTen, "tbTen") &&
    kiemTraRong(user.hoTen, "tbTen", "Họ và Tên nhân viên không được để trống");
  isValid =
    isValid & kiemTraEmail(user.email, "tbEmail") &&
    kiemTraRong(user.email, "tbEmail", "Email không được để trống");
  isValid =
    isValid & kiemTraMatKhau(user.matKhau, "tbMatKhau") &&
    kiemTraRong(user.matKhau, "tbMatKhau", "Mật khẩu không được để trống");
  isValid =
    isValid & kiemTraNgayLam(user.ngayLam, "tbNgay") &&
    kiemTraRong(user.ngayLam, "tbNgay", "Ngày làm không được để trống");
  isValid =
    isValid &
      kiemTraRong(
        user.luong,
        "tbLuongCB",
        "Lương cơ bản không được để trống"
      ) && kiemTraLuong(user.luong, "tbLuongCB");
  isValid =
    isValid &
      kiemTraRong(user.gioLam, "tbGiolam", "Giờ làm không được để trống") &&
    kiemTraGioLam(user.gioLam, "tbGiolam");
  isValid = isValid & kiemTraChucVu(user.chucVu, "tbChucVu");
  if (isValid) {
    dsnv.push(user);
    luuLocalStorage();
    renderDSNV(dsnv);
  }
}

function xoaNV(id) {
  console.log("id: ", id);
  var viTri = timKiemViTri(id, dsnv);
  console.log("viTri: ", viTri);
  if (viTri !== -1) {
    dsnv.splice(viTri, 1);
    luuLocalStorage();
    renderDSNV(dsnv);
  }
}

function suaNV(id) {
  var viTri = timKiemViTri(id, dsnv);
  if (viTri == -1) return;
  var data = dsnv[viTri];
  showThongTinLenForm(data);
  document.getElementById("tknv").disabled = true;
}

function capNhat() {
  var data = layThongTinTuForm();
  var viTri = timKiemViTri(data.taiKhoan, dsnv);
  if (viTri == -1) return;
  dsnv[viTri] = data;
  renderDSNV(dsnv);
  luuLocalStorage();
  document.getElementById("tknv").disabled = false;
}
function timNV() {
  var inputSearch = document.getElementById("searchName").value;
  var contentHTML = "";
  for (var i = 0; i < dsnv.length; i++) {
    if (dsnv[i].xepLoai() == inputSearch) {
      var currentNV = dsnv[i];
      var contentTr = `
                <tr>
                    <td>${currentNV.taiKhoan}</td>
                    <td>${currentNV.en}</td>
                    <td>${currentNV.email}</td>
                    <td>${currentNV.ngayLam}</td>
                    <td>${currentNV.chucVu}</td>
                    <td>${currentNV.tongLuong()}</td>
                    <td>${currentNV.xepLoai()}</td>
                    <td>
                        <button class="btn btn-danger" onclick="xoaNV('${
                          currentNV.taiKhoan
                        }')">Xoá</button>
                        <button class="btn btn-primary" id="sua" onclick="suaNhanVien('${
                          currentNV.taiKhoan
                        }')">Sửa</button>
                    </td>
                </tr>
            `;
      contentHTML += contentTr;
    }
  }
  document.getElementById("tableDanhSach").innerHTML = contentHTML;
}
